package com.pixonic.test.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Тесты:
 * 1. Корректное создание и выполнение задачи
 * 2. Корректное соблюдение очереди выполнения (в разное время)
 * 3. Корректное соблюдение очереди выполнения (задачи назначены на одно время)
 * 4. Гарантированное выполнение большого количества задач (задачи ставятся одним потоком)
 * 5. Гарантированное выполнение большого количества задач (задачи ставятся несколькими потоками)
 * 6. Исключение в случае, если callable = null
 * 7. Исключение в случае, если localDateTime = null
 * 8. В случае, если localDateTime было до текущего времени, немедленное его выполнение
 * 9. Проверка погрешности времени выполнения (не более 1мс для "одиночных" задач)
 */
public class TaskSchedulerTest {

    private static final int SIMPLE_TASK_COUNT = 500;
    private static final int LOAD_TASK_COUNT = 1000000;
    private volatile ITaskScheduler taskScheduler;

    private ITaskScheduler getOrCreateTaskScheduler() {
        if (Objects.isNull(taskScheduler)) {
            taskScheduler = new TaskSchedulerImpl();
            ((TaskSchedulerImpl) taskScheduler).start();
        }
        return taskScheduler;
    }

    /**
     * 1. Корректное создание и выполнение задачи
     */
    @Test
    public void taskExecutionTest() {
        AtomicBoolean result = new AtomicBoolean();
        Callable<Void> callable = () -> {
            result.set(true);
            return null;
        };

        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();
        taskScheduler.schedule(callable, LocalDateTime.now().plus(1, ChronoUnit.SECONDS));

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertTrue(result.get());
    }

    /**
     * 2. Корректное соблюдение очереди выполнения (в разное время)
     */
    @Test
    public void correctExecutionOrderByTime() {
        AtomicReference<LocalDateTime> lastExecutionTime = new AtomicReference<>(LocalDateTime.now());
        AtomicBoolean result = new AtomicBoolean(true);

        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();

        for (int i = 0; i < SIMPLE_TASK_COUNT; i++) {
            Callable<Void> callable = () -> {
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime executionTime = lastExecutionTime.getAndSet(now);
                if (now.isBefore(executionTime)) {
                    result.set(false);
                }
                return null;
            };
            taskScheduler.schedule(callable, LocalDateTime.now().plus(1, ChronoUnit.SECONDS). plus((long) (Math.random() * 500), ChronoUnit.MILLIS));
        }

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertTrue(result.get());
    }

    /**
     * 3. Корректное соблюдение очереди выполнения (задачи назначены на одно время)
     */
    @Test
    public void correctExecutionOrderByScheduled() {
        AtomicInteger counter = new AtomicInteger();
        AtomicBoolean result = new AtomicBoolean(true);

        LocalDateTime time = LocalDateTime.now().plus(1, ChronoUnit.SECONDS);
        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();

        for (int i = 0; i < SIMPLE_TASK_COUNT; i++) {
            final int finalI = i;
            Callable<Void> callable = () -> {
                result.compareAndSet(finalI != counter.getAndIncrement(), false);
                return null;
            };
            taskScheduler.schedule(callable, time);
        }

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertTrue(result.get());
    }

    /**
     * 4. Гарантированное выполнение большого количества задач (задачи ставятся одним потоком)
     */
    @Test
    public void guaranteeExecutionTest() {
        AtomicInteger counter = new AtomicInteger();

        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();

        for (int i = 0; i < LOAD_TASK_COUNT; i++) {
            Callable<Void> callable = () -> {
                counter.incrementAndGet();
                return null;
            };

            taskScheduler.schedule(callable, LocalDateTime.now().plus(100, ChronoUnit.MILLIS));
        }

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertEquals(LOAD_TASK_COUNT, counter.get());
    }

    /**
     * 5. Гарантированное выполнение большого количества задач (задачи ставятся несколькими потоками)
     */
    @Test
    public void guaranteeConcurrentExecutionTest() throws InterruptedException {
        AtomicInteger counter = new AtomicInteger();

        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();
        Thread[] threads = new Thread[4];

        for (int t = 0; t < threads.length; t++) {
            threads[t] = new Thread(() -> {
                for (int i = 0; i < LOAD_TASK_COUNT; i++) {
                    Callable<Void> callable = () -> {
                        counter.incrementAndGet();
                        return null;
                    };

                    taskScheduler.schedule(callable, LocalDateTime.now().plus(80, ChronoUnit.MILLIS));
                }
            });
            threads[t].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertEquals(threads.length * LOAD_TASK_COUNT, counter.get());
    }

    /**
     * 6. Исключение в случае, если callable = null
     */
    @Test(expected = IllegalArgumentException.class)
    public void nullCallableTest() {
        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();
        taskScheduler.schedule(null, LocalDateTime.now());
    }

    /**
     * 7. Исключение в случае, если localDateTime = null
     */
    @Test(expected = IllegalArgumentException.class)
    public void nullLocalDateTimeTest() {
        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();
        taskScheduler.schedule(() -> null, null);
    }

    /**
     * 8. В случае, если localDateTime было до текущего времени, немедленное его выполнение
     */
    @Test
    public void previousLocalDateTimeTest() {
        AtomicBoolean result = new AtomicBoolean();
        AtomicReference<LocalDateTime> executedTime = new AtomicReference<>();
        Callable<Void> callable = () -> {
            executedTime.set(LocalDateTime.now());
            result.set(true);
            return null;
        };

        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();
        LocalDateTime localDateTime = LocalDateTime.now().minus(10, ChronoUnit.SECONDS);
        LocalDateTime scheduledTime = LocalDateTime.now();
        taskScheduler.schedule(callable, localDateTime);

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        Assert.assertTrue(result.get());
        Assert.assertTrue(scheduledTime.until(executedTime.get(), ChronoUnit.MILLIS) < 5);
    }

    /**
     * 9. Проверка погрешности времени выполнения (не более 1мс для "одиночных" задач)
     */
    @Test
    public void executionTimeTest() {
        AtomicLong deltaTime = new AtomicLong();
        ITaskScheduler taskScheduler = getOrCreateTaskScheduler();

        for (int i = 0; i < SIMPLE_TASK_COUNT; i++) {
            LocalDateTime scheduledTime = LocalDateTime.now().plus(100, ChronoUnit.MILLIS).plus(5 * i, ChronoUnit.MILLIS);
            Callable<Void> callable = () -> {
                deltaTime.addAndGet(Math.abs(scheduledTime.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
                return null;
            };
            taskScheduler.schedule(callable, scheduledTime);
        }

        while (taskScheduler.hasScheduledTasks()) {
            Thread.yield();
        }

        System.out.println("Delta (ms): " + deltaTime.get());
        Assert.assertTrue(deltaTime.get() / SIMPLE_TASK_COUNT < 1);
    }
}
