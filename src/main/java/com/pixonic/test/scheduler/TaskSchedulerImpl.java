package com.pixonic.test.scheduler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskSchedulerImpl extends Thread implements ITaskScheduler {
    private final Queue<ScheduledQueueEntry> queue = new PriorityQueue<>(ScheduledQueueEntry::compareTo);
    private AtomicInteger pointer = new AtomicInteger();
    private volatile ScheduledQueueEntry next;

    @Override
    public void schedule(Callable callable, LocalDateTime localDateTime) {
        if (Objects.isNull(callable) || Objects.isNull(localDateTime)) {
            throw new IllegalArgumentException("One of arguments is null");
        }
        ScheduledQueueEntry sqe = new ScheduledQueueEntry(callable, localDateTime);
        sqe.setId(pointer.getAndIncrement());
        synchronized (queue) {
            queue.add(sqe);
            queue.notify();
        }
    }

    @Override
    public boolean hasScheduledTasks() {
        synchronized (queue) {
            return !queue.isEmpty() || next != null;
        }
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {

            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        System.err.println("Thread is interrupted, shutting down");
                        return;
                    }
                }

                next = queue.poll();
            }

            long delay = LocalDateTime.now().until(next.getTime(), ChronoUnit.MILLIS);

            if (delay > 0) {
                synchronized (queue) {
                    try {
                        queue.wait(delay);
                    } catch (InterruptedException e) {
                        System.err.println("Thread is interrupted, shutting down");
                        return;
                    }

                    delay = LocalDateTime.now().until(next.getTime(), ChronoUnit.MILLIS);
                    if (delay > 0) {
                        //Если поток проснулся раньше, чем надо идти дальше -- в него добавили еще один элемент. А вдруг его надо выполнить раньше?
                        queue.add(next);
                        next = null;
                        continue;
                    }
                }
            }

            execute(next.getCallable());
            next = null;
        }
    }

    private void execute(Callable callable) {
        try {
            callable.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
