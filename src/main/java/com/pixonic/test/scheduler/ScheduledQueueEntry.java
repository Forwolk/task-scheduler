package com.pixonic.test.scheduler;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

public class ScheduledQueueEntry implements Comparable<ScheduledQueueEntry> {
    private int id;
    private LocalDateTime time;
    private Callable<?> callable;

    ScheduledQueueEntry(Callable<?> callable, LocalDateTime time) {
        this.callable = callable;
        this.time = time;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Callable<?> getCallable() {
        return callable;
    }

    /**
     * Сортировка сначала по времени выполнения, потом по очереди "вставки"
     */
    @Override
    public int compareTo(ScheduledQueueEntry o) {
        int timeComparing = time.compareTo(o.time);
        return timeComparing == 0 ? id - o.id : timeComparing;
    }
}
