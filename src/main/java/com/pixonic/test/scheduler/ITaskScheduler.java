package com.pixonic.test.scheduler;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

public interface ITaskScheduler {

    /**
     * Создания задания. В назначенный LocalDateTime вызовится Callable
     */
    void schedule(Callable callable, LocalDateTime localDateTime);

    /**
     * Проверка на наличие задач в планировщике
     * @return true, если есть задачи, иначе false
     */
    boolean hasScheduledTasks();
}
